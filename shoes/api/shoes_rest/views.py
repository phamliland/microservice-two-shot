from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO
# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href"
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["model_name", "id"]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "url",
        "bin_var",
    ]
    encoders = {
        "bin_var": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_pk=None):
    if request.method == "GET":
        if bin_vo_pk is not None:
            shoes = Shoe.objects.filter(bin_var=bin_vo_pk)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = f'/api/bins/{content["bin_var"]}/'
            bin_var = BinVO.objects.get(import_href=bin_href)
            content["bin_var"] = bin_var
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(pk=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "bin_var" in content:
                bin_var = BinVO.objects.get(pk=content["bin_var"])
                content["bin_var"] = bin_var
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400
            )
        Shoe.objects.filter(pk=pk).update(**content)
        shoe = Shoe.objects.get(pk=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
