from django.urls import path
from .views import api_list_hats, api_hat_details

urlpatterns = [
    path('hats/<int:pk>/', api_hat_details, name='api_hat_details'),
    path('hats/', api_list_hats, name='api_list_hats'),
    path('locations/<int:location_vo_pk>/hats/', api_list_hats, name='api_show_hats'),
]


