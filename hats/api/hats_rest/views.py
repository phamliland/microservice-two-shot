from .models import Hats, LocationVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'import_href',
        'closet_name',
    ]


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        'fabric',
        'style',
        'color',
        'picture_url',
        'location',
    ]
    encoders = {
        'location': LocationVOEncoder(),
    }


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        'id',
        'style',
        'fabric',
        'color',
        'picture_url',
        'location',
    ]
    encoders = {
        'location': LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_pk=None):
    if request.method == "GET":
        if location_vo_pk is not None:
            hats = Hats.objects.filter(location=location_vo_pk)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {'hats': hats},
            encoder=HatsListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        try:
            href = f'/api/locations/{location_vo_pk}/'
            location = LocationVO.objects.get(import_href=href)
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid location id'},
                status=400,
            )
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_hat_details(request, pk):

    if request.method == "GET":
        hats = Hats.objects.get(pk=pk)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(pk=pk).delete()
        return JsonResponse({'deleted': count > 0})
    else:
        content = json.loads(request.body)
        Hats.objects.filter(pk=pk).update(**content)
        hats = Hats.objects.get(pk=pk)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )
