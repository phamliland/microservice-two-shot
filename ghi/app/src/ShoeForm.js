import React, { useEffect, useState } from 'react'


function ShoeForm() {
    const [manufacturer, setManufacturer] = useState ('');
    const [bins, setBins] = useState ([]);
    const [modelName, setModelName] = useState ('');
    const [color, setColor] = useState ('');
    const [url, setUrl] = useState ('');
    const [bin, setBin] = useState ('');

    const handleManufacturerChange = (event) => {
        const value= event.target.value;
        setManufacturer(value);
    }

    const handleModelNameChange = (event) => {
        const value= event.target.value;
        setModelName(value);
    }

    const handleColorChange = (event) => {
        const value= event.target.value;
        setColor(value);
    }

    const handleUrlChange = (event) => {
        const value= event.target.value;
        setUrl(value);
    }

    const handleBinChange = (event) => {
        const value= event.target.value;
        setBin(value);
        console.log(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data["model_name"] = modelName;
        data["manufacturer"] = manufacturer;
        data["bin_var"] = bin;
        data["color"] = color;
        data["url"] = url;
        console.log(data);

        // const value = document.getElementById('bins').value
        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch (shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe)

            setManufacturer('');
            setModelName('');
            setColor('');
            setUrl('');
            setBin('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create new shoe listing</h1>
              <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                  <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                  <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleModelNameChange} value={modelName} placeholder="Model Name" required type="model_name" name="model_name" id="model_name" className="form-control" />
                  <label htmlFor="model_name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleColorChange} value={color} placeholder="Color" type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleUrlChange} value={url} placeholder="Url" required type="url" name="url" id="url" className="form-control"/>
                  <label htmlFor="url">URL</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleBinChange} value={bin} className="form-select" required name="bin_var" id="bin_var">
                    <option value="">Choose a bin</option>
                    {bins.map(bin => {
                        return (
                            <option key={bin.href} value={bin.id}>
                                {bin.closet_name}
                            </option>
                        );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
}
export default ShoeForm
