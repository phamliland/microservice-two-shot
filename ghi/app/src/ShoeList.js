import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';

function ShoeList() {

    const [shoes, setShoes] = useState([])


    const fetchData = async ()=> {
        const url = 'http://localhost:8080/api/shoes/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
            console.log(data)
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    const deleteShoe = async (id) => {
        const url = `http://localhost:8080/api/shoes/${id}/`;

        const response = await fetch(url, { method: 'DELETE' });
        if (response.ok) {

          setShoes(shoes.filter((shoe) => shoe.id !== id));
        } else {
          console.error('Failed to delete shoe');
        }
      };


    return (
        <>
        <div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Manufacturer</th>
                <th>Model Name</th>
                <th>Color</th>
                <th>Bin</th>
                <th>Picture</th>
            </tr>
            </thead>
            <tbody>
            {shoes.map((shoe) => {
            return (
                <tr key={shoe.href} value={shoe.href}>
                    <td>{shoe.manufacturer}</td>
                    <td>{shoe.model_name}</td>
                    <td>{shoe.color}</td>
                    <td>{shoe.bin_var.closet_name}</td>
                    <td>
                        <img src={shoe.url} width="75px" height="100px"/>
                    </td>
                    <td>
                        <button onClick={() => deleteShoe(shoe.id)}>Delete</button>
                    </td>
                </tr>
            );
        })}
            </tbody>
        </table>
        </div>
        <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Track new shoe</Link>
        </>
    )
    }

export default ShoeList;
